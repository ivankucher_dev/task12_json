package com.epam.trainings.mvc.model.xmlmodels;

import java.io.Serializable;
import java.util.List;

public class GunsList implements Serializable {

  List<Gun> guns;

  public GunsList() {}

  public List<Gun> getGuns() {
    return guns;
  }

  public void setGuns(List<Gun> guns) {
    this.guns = guns;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    guns.forEach(gun -> sb.append(gun + "\n"));
    return sb.toString();
  }
}
