package com.epam.trainings.mvc.model.xmlmodels;

import static com.epam.trainings.utils.PropertiesReader.*;

public class TTC {
  private static final int LOW_RANGE = 500;
  private static final int MIDDLE_RANGE = 1000;
  private int firingRange;
  private int sightingRange;
  private boolean magazine;
  private boolean optic;

  public TTC(
      int firingRange, int sightingRange, boolean magazineAvaliability, boolean opticAvaliability) {
    this.firingRange = firingRange;
    this.sightingRange = sightingRange;
    this.magazine = magazineAvaliability;
    this.optic = opticAvaliability;
  }

  public TTC() {}

  public void setFiringRange(int firingRange) {
    this.firingRange = firingRange;
  }

  public void setSightingRange(int sightingRange) {
    this.sightingRange = sightingRange;
  }

  public void setMagazine(boolean magazine) {
    this.magazine = magazine;
  }

  public void setOptic(boolean optic) {
    this.optic = optic;
  }

  public String isOpticAvaliability() {
    if (optic) {
      return "YES";
    } else {
      return "NO";
    }
  }

  public String isMagazineAvaliability() {
    if (magazine) {
      return "YES";
    } else {
      return "NO";
    }
  }

  public int getFiringRange() {
    return firingRange;
  }

  public int getSightingRange() {
    return sightingRange;
  }

  public String getStringSightingRange() {
    return this.sightingRange + " m.";
  }

  public String getStringFiringRange() {
    if (firingRange < LOW_RANGE) {
      return getProperty("low_range") + " (" + firingRange + " m.)";
    } else if (firingRange < MIDDLE_RANGE) {
      return getProperty("medium_range") + " (" + firingRange + " m.)";
    } else if (firingRange > MIDDLE_RANGE) {
      return getProperty("high_range") + " (" + firingRange + " m.)";
    } else return "not_found";
  }

  @Override
  public String toString() {
    return "\nTTC :\nfiring range : "
        + getStringFiringRange()
        + "\nsighting range : "
        + getStringSightingRange()
        + "\nMagazine avaliability : "
        + isMagazineAvaliability()
        + "\nOptic avaliability : "
        + isOpticAvaliability()
        + "\n";
  }
}
