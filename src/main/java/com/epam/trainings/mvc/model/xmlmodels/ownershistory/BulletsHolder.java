package com.epam.trainings.mvc.model.xmlmodels.ownershistory;

import com.epam.trainings.mvc.model.xmlmodels.Bullet;
import java.util.List;

public class BulletsHolder {
  private List<Bullet> bullets;

  public List<Bullet> getBullets() {
    return bullets;
  }

  public void setBullets(List<Bullet> bullets) {
    this.bullets = bullets;
  }
}
