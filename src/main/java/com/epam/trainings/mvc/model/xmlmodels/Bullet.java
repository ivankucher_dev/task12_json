package com.epam.trainings.mvc.model.xmlmodels;

public class Bullet {
  private double calibr;
  private String factoryName;
  private String type;

  public Bullet(double calibr, String factoryName, String type) {
    this.calibr = calibr;
    this.factoryName = factoryName;
    this.type = type;
  }

  public Bullet() {}

  public double getCalibr() {
    return calibr;
  }

  public void setCalibr(double calibr) {
    this.calibr = calibr;
  }

  public String getFactoryName() {
    return factoryName;
  }

  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "{ Bullet calibr - "
        + calibr
        + ", factory - "
        + factoryName
        + ", type of bullets - "
        + type;
  }
}
