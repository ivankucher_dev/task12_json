package com.epam.trainings.mvc.model.xmlmodels.ownershistory;

public class Owner {

  private String name;
  private String surname;
  private String location;
  private long pasportNumber;
  private HistoryInfo info;

  public Owner(String name, String surname, String location, long pasportNumber) {
    this.name = name;
    this.surname = surname;
    this.location = location;
    this.pasportNumber = pasportNumber;
  }

  public Owner() {}

  public HistoryInfo getInfo() {
    return info;
  }

  public void setInfo(HistoryInfo info) {
    this.info = info;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public long getPasportNumber() {
    return pasportNumber;
  }

  public void setPasportNumber(long pasportNumber) {
    this.pasportNumber = pasportNumber;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n{Owner - " + name + " " + surname);
    sb.append(", location - " + location);
    sb.append(", pasport number - " + pasportNumber);
    sb.append("\nhistory info : \n[ " + info);
    sb.append("]");
    return sb.toString();
  }
}
