package com.epam.trainings.mvc.model.xmlmodels;

import com.epam.trainings.mvc.model.xmlmodels.ownershistory.Owner;
import static com.epam.trainings.utils.PropertiesReader.*;
import java.util.ArrayList;
import java.util.List;

public class Gun {

  private String model;
  private List<Owner> ownershipsHistory;
  private int handy;
  private String origin;
  private TTC ttc;
  private List<Bullet> bullets;
  private String material;

  public Gun(
      String model, int handy, String origin, TTC ttc, List<Bullet> bullets, String material) {
    this.model = model;
    this.handy = handy;
    this.origin = origin;
    this.ttc = ttc;
    this.bullets = bullets;
    this.material = material;
  }

  public Gun() {
    ownershipsHistory = new ArrayList<>();
  }

  public List getOwnershipsHistory() {
    return ownershipsHistory;
  }

  public void setOwnershipsHistory(List<Owner> ownershipsHistory) {

    this.ownershipsHistory = ownershipsHistory;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public int getHandy() {
    return handy;
  }

  public String getStringHandy() {
    if (handy == getIntProp("one_handy")) {
      return getProperty("one_handy_string");
    } else if (handy == getIntProp("two_handy")) {
      return getProperty("two_handy_string");
    } else {
      return "not_found";
    }
  }

  public void setHandy(int handy) {
    this.handy = handy;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public TTC getTtc() {
    return ttc;
  }

  public void setTtc(TTC ttc) {
    this.ttc = ttc;
  }

  public List<Bullet> getBullets() {
    return bullets;
  }

  public void setBullets(List<Bullet> bullets) {
    this.bullets = bullets;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{ Gun model - " + model);
    sb.append("\nOwners history :\n[");
    ownershipsHistory.forEach(owner -> sb.append(owner));
    sb.append("\n]");
    sb.append("\nhandies - " + handy + ", country of origin - " + origin);
    sb.append(ttc);
    sb.append("Bullets :\n[");
    bullets.forEach(bullet -> sb.append(bullet + "\n"));
    sb.append("\n]\nmaterial - " + material + "}\n\n");
    return sb.toString();
  }
}
