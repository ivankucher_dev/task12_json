package com.epam.trainings.mvc.view;

import com.epam.trainings.mvc.commands.GsonParseCommand;
import com.epam.trainings.mvc.commands.JacksonParseCommand;
import com.epam.trainings.mvc.commands.ToJsonCommand;
import com.epam.trainings.mvc.commands.ValidateCommand;
import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.model.Menu;
import com.fasterxml.jackson.core.JsonParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.InputMismatchException;
import java.util.Scanner;
import static com.epam.trainings.utils.PropertiesReader.*;

public class View {

  private Menu menu;
  private ViewController controller;
  private Scanner scanner;
  private static Logger log = LogManager.getLogger(View.class.getName());
  private File xmlFile = new File("src\\main\\resources\\xml\\guns.xml");


  public View(Menu menu) {
    this.menu = menu;
    scanner = new Scanner(System.in);
  }

  public void startBySettingUpController(ViewController controller) {
    this.controller = controller;
    createMenu();
    show();
  }

  public void updateView() {
    show();
  }

  private void show() {
    menu.getMenuAsString().forEach((k, v) -> log.info(k + "." + v));
    int index = -1;
    try {
      index = scanner.nextInt();
    } catch (InputMismatchException e) {
      log.error(e);
      show();
    }
    controller.execute(menu.getCommand(index));
  }

  private void createMenu() {
    menu.add(1,getProperty("validate_command"),new ValidateCommand());
    menu.add(2,getProperty("validate_command"),new JacksonParseCommand());
    menu.add(3,getProperty("validate_command"),new GsonParseCommand());
    menu.add(4,getProperty("validate_command"),new ToJsonCommand());
  }
}
