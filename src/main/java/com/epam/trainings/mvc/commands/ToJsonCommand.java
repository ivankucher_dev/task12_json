package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.xmlmodels.GunsList;
import com.epam.trainings.utils.PropertiesReader;
import com.google.gson.Gson;
import java.io.*;
import java.util.Properties;

public class ToJsonCommand implements Command {
  private static Properties properties =
      PropertiesReader.getPropertiesFile("properties/files.properties");

  @Override
  public void execute() {
    GsonParseCommand getGunsFromJson = new GsonParseCommand();
    getGunsFromJson.execute();
    GunsList guns = getGunsFromJson.getGuns();
    Gson gson = new Gson();
    try (FileWriter writer = new FileWriter(properties.getProperty("sorted_json_path"))) {
      gson.toJson(guns, writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
