package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.xmlmodels.GunsList;
import com.epam.trainings.utils.PropertiesReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Properties;

public class JacksonParseCommand implements Command {
  private static Properties properties =
      PropertiesReader.getPropertiesFile("properties/files.properties");

  @Override
  public void execute() {
    ObjectMapper mapper = new ObjectMapper();
    GunsList guns = null;
    try {
      guns =
          mapper.readValue(
              getClass().getResource(properties.getProperty("json_path")), GunsList.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    guns.getGuns().forEach(gun -> System.out.println(gun));
  }
}
