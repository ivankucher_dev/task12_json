package com.epam.trainings.mvc.commands;

import com.epam.trainings.utils.GunJsonValidator;

public class ValidateCommand implements Command {
  @Override
  public void execute() {
    GunJsonValidator.validate();
  }
}
