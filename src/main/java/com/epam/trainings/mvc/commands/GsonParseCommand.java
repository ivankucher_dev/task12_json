package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.xmlmodels.GunsList;
import com.epam.trainings.utils.GunComparator;
import com.epam.trainings.utils.PropertiesReader;
import com.google.gson.Gson;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class GsonParseCommand implements Command {

  GunsList guns;

  public GsonParseCommand() {
    guns = new GunsList();
  }

  private static Properties properties =
      PropertiesReader.getPropertiesFile("properties/files.properties");

  @Override
  public void execute() {
    Gson gson = new Gson();
    InputStream inputStream = getClass().getResourceAsStream(properties.getProperty("json_path"));
    InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
    guns = gson.fromJson(streamReader, GunsList.class);
    guns.getGuns().sort(new GunComparator());
    guns.getGuns().forEach(gun -> System.out.println(gun));
  }

  public GunsList getGuns() {
    return guns;
  }
}
