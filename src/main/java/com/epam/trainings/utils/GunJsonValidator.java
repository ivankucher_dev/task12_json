package com.epam.trainings.utils;

import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import org.everit.json.schema.Schema;

import java.util.Properties;

public class GunJsonValidator {

  private static Properties properties =
      PropertiesReader.getPropertiesFile("properties/files.properties");

  public static void validate() {
    JSONObject jsonSchema =
        new JSONObject(
            new JSONTokener(
                GunJsonValidator.class.getResourceAsStream(properties.getProperty("json_path"))));
    JSONObject jsonSubject =
        new JSONObject(
            new JSONTokener(
                GunJsonValidator.class.getResourceAsStream(
                    properties.getProperty("json_schema_path"))));
    Schema schema = SchemaLoader.load(jsonSchema);
    try {

      schema.validate(jsonSubject);
    } catch (ValidationException e) {
      e.printStackTrace();
    }
  }
}
